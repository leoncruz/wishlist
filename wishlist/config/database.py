import os

from dotenv import load_dotenv
from sqlalchemy import create_engine

load_dotenv(override=False)

user = os.getenv("MYSQL_USER")
password = os.getenv("MYSQL_PASSWORD")
host = os.getenv("MYSQL_HOST")
port = os.getenv("MYSQL_PORT")

if os.getenv("ENV") == "TEST":
    database = "wishlist_test"
else:
    database = os.getenv("MYSQL_DATABASE")

URI = f"mysql+pymysql://{user}:{password}@{host}:{port}/{database}"
engine = create_engine(URI, echo=True)
