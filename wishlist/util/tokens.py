import datetime

import falcon
import jwt

secret = "secret"


def generate_token(user_email):
    """Gera um token com expireção de 10 minutos."""
    return jwt.encode(
        {
            "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=10),
            "user": user_email,
        },
        secret,
        algorithm="HS256",
    )


def validate_token(token):
    return jwt.decode(token, secret, algorithms="HS256")


def token_required(req, resp, resource, param):
    header = req.headers
    try:
        token = header["AUTHORIZATION"].split(" ")[1]
        validate_token(token)
    except KeyError as e:
        msg = "O Token de acesso é necessário."
        raise falcon.HTTPUnauthorized("Não autorizado", msg)
    except jwt.exceptions.ExpiredSignatureError as e:
        msg = "Token expirado."
        raise falcon.HTTPUnauthorized("Não autorizado", msg)
    except jwt.exceptions.DecodeError as e:
        msg = "Token inválido."
        raise falcon.HTTPUnauthorized("Não autorizado", msg)
