import json

import jwt
from wishlist.util.tokens import validate_token


class BaseResouce:
    def to_json(self, data):
        return json.dumps(data)

    def from_json(self, data):
        try:
            return json.loads(data)
        except json.decoder.JSONDecodeError as e:
            return False

    def params(self, req):
        return self.from_json(req.stream.read())

    def get_token(self, req):
        header = req.headers
        auth = header["AUTHORIZATION"]
        token = auth.split(" ")[1]
        return validate_token(token)
