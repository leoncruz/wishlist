from dataclasses import asdict

import falcon
from sqlalchemy.orm.exc import NoResultFound
from wishlist.app.models import Product, User, WishList, session
from wishlist.util.base_resource import BaseResouce
from wishlist.util.tokens import token_required


@falcon.before(token_required)
class WishlistResource(BaseResouce):
    def on_get_all(self, req, resp):
        token = self.get_token(req)
        email = token["user"]

        user = session.query(User).filter(User.email == email).one()

        wishlists = []
        for wishlist in user.wishlists:
            wishlists.append(asdict(wishlist))

        resp.body = self.to_json(wishlists)

    def on_get(self, req, resp, wishlist_id):
        token = self.get_token(req)
        email = token["user"]

        user = session.query(User).filter(User.email == email).one()
        wishlist_query = (
            session.query(WishList)
            .filter(WishList.user_id == user.id, WishList.id == wishlist_id)
            .one()
        )

        wishlist = {
            "id": wishlist_query.id,
            "title": wishlist_query.title,
            "products": [],
        }
        for product in wishlist_query.products:
            wishlist["products"].append((asdict(product)))

        resp.body = self.to_json(wishlist)

    def on_post(self, req, resp):
        token = self.get_token(req)
        email = token["user"]
        data = self.params(req)

        try:
            title = data["title"]
        except TypeError as e:
            resp.status = falcon.HTTP_406
            resp.body = self.to_json({"erro": "A lista tem que ter um título."})
            return resp

        user = session.query(User).filter(User.email == email).one()
        wishlist = WishList(user_id=user.id, title=title)

        try:
            session.add(wishlist)
            session.commit()
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = self.to_json(
                {"erro": "Um erro ocorreu ao processar a operação."}
            )
            return resp

        resp.body = self.to_json({"sucesso": "Lista de desejos criada com sucesso."})
        resp.status = falcon.HTTP_201

    def on_delete(self, req, resp, wishlist_id):
        token = self.get_token(req)
        email = token["user"]

        try:
            user = session.query(User).filter(User.email == email).one()
        except NoResultFound as e:
            resp.status = falcon.HTTP_404
            resp.body = self.to_json({"erro": "Usuário não encontrado."})

        try:
            wishlist = (
                session.query(WishList)
                .filter(WishList.user_id == user.id, WishList.id == wishlist_id)
                .one()
            )
        except NoResultFound as e:
            resp.status = falcon.HTTP_404
            resp.body = self.to_json({"erro": "Lista de desejos não encontrada."})

        try:
            session.delete(wishlist)
            session.commit()
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = self.to_json({"erro": "Um erro ocorreu ao processar operação."})
            return resp
