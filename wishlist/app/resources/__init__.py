from .accounts import AccountsResource
from .products import ProductsResource
from .wishlists import WishlistResource
