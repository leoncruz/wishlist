import falcon
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from wishlist.app.models import User, session
from wishlist.util.base_resource import BaseResouce
from wishlist.util.tokens import generate_token


class AccountsResource(BaseResouce):
    def on_post_register(self, req, resp):
        data = self.params(req)

        try:
            name = data["name"]
            email = data["email"]
            username = data["username"]
            password = data["password"]
        except (TypeError, KeyError) as e:
            resp.body = self.to_json({"erro": "Os campos não podem ser vazios."})
            return resp

        new_user = User(name=name, email=email, username=username, password=password)

        if new_user.password is not None:
            new_user.hash_password(password)

        try:
            session.add(new_user)
            session.commit()
            resp.body = self.to_json({"sucesso": "Usuário criado"})
            resp.status = falcon.HTTP_201
            return resp
        except IntegrityError as e:
            session.rollback()
            resp.body = self.to_json({"erro": "O email já está cadastrado."})
            return resp

    def on_post_sign_in(self, req, resp):
        data = self.params(req)

        try:
            email = data["email"]
            password = data["password"]
        except (KeyError, TypeError) as e:
            resp.body = self.to_json({"erro": "Os campos não podem ser vazios."})
            return resp

        try:
            user = session.query(User).filter(User.email == email).one()
        except NoResultFound as e:
            resp.body = self.to_json({"erro": "Usuário não encontrado."})
            return resp

        if user.check_password(password):
            token = generate_token(email)
            resp.body = self.to_json({"token": token})
