from dataclasses import asdict
from random import choice

import falcon
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
from wishlist.app.models import Product, User, WishList, session
from wishlist.util.base_resource import BaseResouce
from wishlist.util.tokens import token_required


class ProductsResource(BaseResouce):
    @falcon.before(token_required)
    def on_post_add_product(self, req, resp, wishlist_id):
        data = self.params(req)

        try:
            title = data["title"]
            status = data["status"]
        except TypeError as e:
            resp.status = falcon.HTTP_406
            resp.body = self.to_json(
                {"erro": "O produto tem que ter um título e um status."}
            )
            return resp

        if status == "não recebido":
            status = False
        elif status == "recebido":
            status = True

        product = Product(title=title, status=status, wishlist_id=wishlist_id)

        try:
            session.add(product)
            session.commit()
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = self.to_json({"erro": "Um erro ocorreu ao processar operação."})
            return resp

        resp.status = falcon.HTTP_201
        resp.body = self.to_json({"sucesso": "Produto adicionado com sucesso."})

    @falcon.before(token_required)
    def on_patch(self, req, resp, product_id):
        token = self.get_token(req)
        email = token["user"]
        status = self.params(req)["status"]

        try:
            user = session.query(User).filter(User.email == email).one()
        except NoResultFound as e:
            resp.status = falcon.HTTP_404
            resp.body = self.to_json({"erro": "Usuário não encontrado."})
            return resp

        try:
            product = (
                session.query(Product)
                .join(WishList)
                .filter(WishList.user_id == user.id, Product.id == product_id)
                .one()
            )
        except NoResultFound as e:
            resp.status = falcon.HTTP_404
            resp.body = self.to_json({"erro": "Produto não encontrado."})
            return resp

        if status == "recebido":
            product.status = True
        elif status == "não recebido":
            product.status = False
        else:
            resp.status = falcon.HTTP_400
            resp.body = self.to_json({"erro": "O status é inválido."})
            return resp

        try:
            session.commit()
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = self.to_json({"erro": "Um erro ocorreu ao processar operação"})
            return resp

        resp.status = falcon.HTTP_200
        resp.body = self.to_json(asdict(product))

    @falcon.before(token_required)
    def on_delete(self, req, resp, product_id):
        token = self.get_token(req)
        email = token["user"]

        try:
            user = session.query(User).filter(User.email == email).one()
        except NoResultFound as e:
            resp.status = falcon.HTTP_404
            resp.body = self.to_json({"erro": "Usuário não encontrado."})
            return resp

        try:
            product = (
                session.query(Product)
                .join(WishList)
                .filter(WishList.user_id == user.id, Product.id == product_id)
                .one()
            )
        except NoResultFound as e:
            resp.status = falcon.HTTP_404
            resp.body = self.to_json({"erro": "Produto não encontrado."})
            return resp

        try:
            session.delete(product)
            session.commit()
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = self.to_json(
                {"erro": "Um error ocorreu ao processar operação."}
            )
            return resp

        resp.status = falcon.HTTP_204

    def on_get_product_random(self, req, resp, username):
        """Função que retorna um produto aleatório de qualquer lista do usuário."""
        try:
            user = session.query(User).filter(User.username == username).one()
        except NoResultFound as e:
            resp.body = self.to_json({"erro": "Usuário não encontrado."})
            resp.status = falcon.HTTP_404
            return resp

        products = (
            session.query(Product)
            .join(WishList)
            .filter(WishList.user_id == user.id)
            .all()
        )

        try:
            product_random = choice(products)
        except IndexError as e:
            resp.body = self.to_json(
                {"erro": "O Usuário ainda não adicionou presentes a nenhuma lista."}
            )
            resp.status = falcon.HTTP_404
            return resp

        resp.body = self.to_json(asdict(product_random))

    def on_get_product_random_wishlist(self, req, resp, username, wishlist_id):
        """Função que retorna um produto aleatório de uma lista especifica do usuário."""
        try:
            user = session.query(User).filter(User.username == username).one()
            wishlist = (
                session.query(WishList)
                .filter(WishList.id == wishlist_id, WishList.user_id == user.id)
                .one()
            )
        except NoResultFound as e:
            resp.body = self.to_json(
                {"erro": "Usuário ou lista de desejos não encontrados."}
            )
            resp.status = falcon.HTTP_404
            return resp

        products = session.query(Product).filter(Product.wishlist_id == wishlist.id)

        try:
            product = choice(list(products))
        except IndexError as e:
            resp.body = self.to_json(
                {"erro": "O Usuário ainda não adicionou presentes a lista."}
            )
            resp.status = falcon.HTTP_404
            return resp

        resp.body = self.to_json(asdict(product))
