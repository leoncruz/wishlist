from dataclasses import dataclass

import bcrypt
import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, validates
from wishlist.config.database import engine

Base = declarative_base()
Session = sessionmaker(bind=engine)


class User(Base):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250), nullable=False)
    email = db.Column(db.String(250), nullable=False, unique=True)
    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(350), nullable=False)
    wishlists = relationship("WishList", backref="wishlists")

    def hash_password(self, password):
        self.password = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

    def check_password(self, password):
        return bcrypt.checkpw(password.encode(), self.password.encode())


@dataclass
class WishList(Base):
    __tablename__ = "wishlists"

    id: int
    title: str
    user_id: int

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    user = relationship("User")
    products = relationship("Product", backref="products", cascade="all, delete")


@dataclass
class Product(Base):
    __tablename__ = "products"

    id: int
    title: str
    status: bool
    description: str
    link: str
    image: str
    wishlist_id: int

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250), nullable=True)
    status = db.Column(db.Boolean(), nullable=False, default=False)
    description = db.Column(db.Text(), nullable=True)
    link = db.Column(db.String(250), nullable=True)
    image = db.Column(db.String(250), nullable=True)
    wishlist_id = db.Column(db.Integer, db.ForeignKey("wishlists.id"))
    wishlist = relationship("WishList")


Base.metadata.create_all(engine)
session = Session()
