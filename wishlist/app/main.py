import falcon
from wishlist.app.resources import AccountsResource, ProductsResource, WishlistResource

api = application = falcon.API()

accounts_resource = AccountsResource()
wishlists_resource = WishlistResource()
products_resource = ProductsResource()

api.add_route("/accounts/register", accounts_resource, suffix="register")
api.add_route("/accounts/login", accounts_resource, suffix="sign_in")

api.add_route("/wishlists", wishlists_resource, suffix="all")
api.add_route("/wishlist/new", wishlists_resource)
api.add_route("/wishlist/{wishlist_id}", wishlists_resource)


api.add_route(
    "/products/{wishlist_id}/add_product", products_resource, suffix="add_product"
)
api.add_route("/product/{product_id}", products_resource)
api.add_route("/product/{product_id}/change_status", products_resource)
api.add_route("/product/random/{username}", products_resource, suffix="product_random")
api.add_route(
    "/product/random/{username}/{wishlist_id}",
    products_resource,
    suffix="product_random_wishlist",
)
