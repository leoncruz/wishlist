import json
from urllib.parse import urlencode

import falcon
from falcon import testing
from wishlist.app.main import api
from wishlist.app.models import User, session


class TestAccountsResource(testing.TestCase):
    def setUp(self):
        super().setUp()

        self.api = api

    def test_create_new_account(self):
        user = urlencode(
            {
                "name": "Usuario Teste",
                "email": "usuario@email.com",
                "username": "usuario.teste",
                "password": "12345",
            }
        )
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        response = self.simulate_post(
            path="/accounts/register",
            body=user,
            headers=headers,
        )
        self.assertEqual(response.status, falcon.HTTP_201)
