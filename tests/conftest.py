import os

import pytest
from falcon import testing
from wishlist.app.main import api


@pytest.fixture
def client():
    return testing.TestClient(api)
