## Wishlist

Para rodar o projeto, clone o repositório utilizando git com o comando:

	git clone https://gitlab.com/leoncruz/wishlist

Instale as dependências do projeto com o comando:

	pip install -r requirements.txt

Ou o comando:

	poetry install

Para executar a aplicação, execute o comando:

	gunicorn --reload wishlist.app.main

>***É preciso ter um banco de dados MySQL com a porta: 3306***

#### URLs de acesso

**/accounts/register -> Cria uma nova conta**

	campos obrigatórios do json a ser enviado: name, email, username, password

**/accounts/login -> Realizar o login do usuário e retorna um token de acesso**

	campos obrigatórios do json a ser enviado: email, password

**/wishlists -> Retorna um json com todas as listas do usuário logado***

**/wishlist/new -> Cria uma nova lista para o usuário logado***

	campo obrigatório do json a ser enviado: title

**/wishlist/{wishlist_id} -> Retorna uma lista dado o id***

**/products/{wishlist_id}/add_product -> Adiciona um produto a uma lista especifica***

	campo obrigatório do json a ser enviado: title

**/product/{product_id} -> Retona um produto especifico***

**/product/{product_id}/change_status -> Altera o status de um produto***

**/product/random/{username} -> Retona um produto de forma aleatória de qualquer lista do usuário**

**/product/random/{username}/{wishlist_id} -> Retona um produto de forma aleatória de uma lista especifica**

>****É preciso passar o token retornado no login no header da requisição***
